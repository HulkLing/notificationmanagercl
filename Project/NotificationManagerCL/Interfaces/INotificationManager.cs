﻿using System.Collections.Generic;

namespace NotificationManagerCL.Interfaces
{
    public interface INotificationManager
    {
        void HandleSignal(string code);
        void AddDestinationForSignal(string email, string code);
        void ExcludeDestinationForSignal(string email, string code);
        List<string> GetSignalList();
        void AddSignal(string code);
        bool IsCheckDestinationForSignal(string email, string code);
    }
}