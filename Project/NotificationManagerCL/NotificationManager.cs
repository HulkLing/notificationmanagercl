﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NotificationManagerCL
{
    /// <summary>
    /// Класс NotificationManager, реализующий интерфейс INotificationManager
    /// </summary>
    public class NotificationManager : Interfaces.INotificationManager
    {
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<string>> Signals { get; private set; }

        /// <summary>
        /// Обработать сигнал
        /// </summary>
        /// <param name="code"></param>
        /// <value>При вызове метода модуль на основании кода сигнала определяет список адресатов 
        /// и выполняет рассылку уведомления о наступлении события с указанным кодом</value>
        public void HandleSignal(string code)
        {
            if (Signals.ContainsKey(code))
                foreach (var destination in Signals[code])
                {
                    Console.WriteLine(destination);

/*
            // отправитель - устанавливаем адрес и отображаемое в письме имя
            MailAddress from = new MailAddress("somemail@gmail.com", "Радик");
            // кому отправляем
            MailAddress to = new MailAddress(addresat);
            // создаем объект сообщения
            MailMessage m = new MailMessage(from, to);
            // тема письма
            m.Subject = "Тест";
            // текст письма
            m.Body = "<h2>Письмо-тест работы smtp-клиента</h2>";
            // письмо представляет код html
            m.IsBodyHtml = true;
            // адрес smtp-сервера и порт, с которого будем отправлять письмо
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            // логин и пароль
            smtp.Credentials = new NetworkCredential("nom", "mypassword");
            smtp.EnableSsl = true;
            smtp.Send(m);*/
                }
        }

        /// <summary>
        /// Добавить адресата для сигнала
        /// </summary>
        /// <param name="email"></param>
        /// <param name="code"></param>
        /// <value>При вызове метода модуль добавляет 
        /// указанного адресата в список адресатов для указанного кода</value>
        public void AddDestinationForSignal(string email, string code)
        {
            if (Signals.ContainsKey(code))
                Signals[code].Add(email);
        }

        /// <summary>
        /// Исключить адресата из сигнала
        /// </summary>
        /// <param name="email"></param>
        /// <param name="code"></param>
        /// <value>При вызове метода модуль исключает указанного
        ///  адресата в список адресатов для указанного кода</value>
        public void ExcludeDestinationForSignal(string email, string code)
        {
            if (Signals.ContainsKey(code))
                Signals[code].Remove(email);
        }

        /// <summary>
        /// Получить список поддерживаемых сигналов
        /// </summary>
        /// <value>При вызове модуль формирует и возвращает список сигналов, 
        /// для которых у него назначены адресаты</value>
        /// <returns></returns>
        public List<string> GetSignalList()
        {
            return Signals.Where(p => p.Value.Count != 0).Select(p => p.Key).ToList();
        }

        /// <summary>
        /// Добавить сигнал
        /// </summary>
        /// <param name="code"></param>
        /// <value>При вызове модуль создаёт пустой список адресатов для указанного кода</value>
        public void AddSignal(string code)
        {
            if (Signals == null)
            {
                Signals = new Dictionary<string, List<string>>();
            }
            if (!Signals.ContainsKey(code))
                Signals.Add(code, new List<string>());
        }

        /// <summary>
        /// Проверить будет ли адресат получать уведомление для сигнала
        /// </summary>
        /// <param name="email">Email адресата</param>
        /// <param name="code">Строковый код сигнала</param>
        /// <value>При вызове модуль проверяет, участвует ли данный адресат в 
        /// рассылке уведомлений для данного сигнала</value>
        /// <returns></returns>
        public bool IsCheckDestinationForSignal(string email, string code)
        {
            return Signals.ContainsKey(code) && Signals[code].Contains(email);
        }
    }
}